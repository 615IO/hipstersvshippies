// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "HipstersVsHippies.h"
#include "HipstersVsHippiesGameMode.h"
#include "HipstersVsHippiesHUD.h"
#include "HipstersVsHippiesCharacter.h"

AHipstersVsHippiesGameMode::AHipstersVsHippiesGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AHipstersVsHippiesHUD::StaticClass();
}
