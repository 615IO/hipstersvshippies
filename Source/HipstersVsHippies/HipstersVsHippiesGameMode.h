// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "HipstersVsHippiesGameMode.generated.h"

UCLASS(minimalapi)
class AHipstersVsHippiesGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AHipstersVsHippiesGameMode();
};



